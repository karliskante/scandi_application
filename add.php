<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
         <title>Products</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
     <link href="style.css" rel="stylesheet">
  </head>
  <body>
    <script src="include/form.js"></script>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href="index.php">Product List</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="add.php">Add Product</a>
          </li>
        </ul>
      </div>
    </nav>
    <div id="container">
       <div id="header">
        <span class="header-title"> Add Product </span>
        <div class="header-action">All Fields are mandatory</div>
        <div class="line"></div>
    </div>
    <?php include("include/insert.php"); ?>
    <form id="myForm" method="post">
     <div class="container-fluid">
       <div class="row">
         <div class="col-lx-4 col-lg-4">
         </div>
         <div class="col-lx-4 col-md-12 col-lg-4">
           <table class="table">
             <tr>
               <td colspan="4">
                    SKU
               </td>
               <td colspan="4">
                 <input class="form-control" id="ksu" type="text" name="ksu">
               </td>
             </tr>

             <tr>
             <td colspan="4">
                 Name
             </td>
             <td colspan="4">
               <input class="form-control" id="name" type="text" name="name">
             </td>
           </tr>
           <td colspan="4">
               Price
           </td>
           <td colspan="4">
             <input class="form-control" id="price" type="text" name="price">
           </td>

         </tr>
         <td colspan="4">
             Type
         </td>
         <td colspan="4">

    <select name="type" id="type" onchange="gettype(this);">
      <option value="" selected></option>
    <option name= "furniture" value="furniture">Furniture</option>
    <option name= "disc" value="disc">Disc</option>
    <option name="book" value="book">Book</option>
    </select>
         </td>
         <tr>
         <td colspan="4">
           <div id="textz">

           </div>
         </td>
         <td colspan="4">
             <input class="form-control" id="attribute" type="hidden" name="attribute" >
             <input class="form-control" id="attribute2" type="hidden" name="attribute2" >
             <input class="form-control" id="attribute3" type="hidden" name="attribute3" >
              <span id="advice"></span>
         </td>
    </tr>
         <tr>
           <td colspan="4">
           </td>
           <td colspan="4">
             <input type="submit" name="submit" value="Save">

           </td>
         </tr>

    </table>
    </div>
    </form>
       </div>
    </div>
    </div>


<footer class="footer">
Kārlis Kante
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
  </body>
</html>
