<?php
$server = "localhost";
$username = "root";
$password = "";
$db = "scandi";

// Create connection
$conn = mysqli_connect($server, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}
// check if submit button is pressed
if(isset($_POST['submit']))
{
   // get data from textboxes in variables
   $ksu = mysqli_real_escape_string($conn, $_REQUEST['ksu']);
   $name = mysqli_real_escape_string($conn, $_REQUEST['name']);
   $price = mysqli_real_escape_string($conn, $_REQUEST['price']);
   $type =  mysqli_real_escape_string($conn, $_REQUEST['type']);
// if type furniture is selected
   if($type == 'furniture') {

     $attribute1 = mysqli_real_escape_string($conn, $_REQUEST['attribute']);
     $attribute2 = mysqli_real_escape_string($conn, $_REQUEST['attribute2']);
     $attribute3 = mysqli_real_escape_string($conn, $_REQUEST['attribute3']);
     // check if all fields are completed
     if (empty($attribute1) || empty($attribute2) || empty($attribute3)) {
       $attribute = '';
     }
     // if yes then merge them in one
     else {
     $attribute = $attribute1 . 'x' . $attribute2 . 'x' . $attribute3;
   }
   }
   // if type is book or disc theres no need to merge anything
   else {
    $attribute = mysqli_real_escape_string($conn, $_REQUEST['attribute']);

}
// validate that all textboxes are completed
  if (empty($ksu)) {
    echo "<script>alert('KSU IS EMPTY');</script>";
  }
  if (empty($name)) {
    echo "<script>alert('NAME IS EMPTY');</script>";
  }
  if (empty($price)) {
    echo "<script>alert('PRICE IS EMPTY');</script>";
  }
  if (empty($type)) {
    echo "<script>alert('TYPE IS EMPTY');</script>";
  }
  if (empty($attribute)) {
    echo "<script>alert('attribute IS EMPTY');</script>";
  }
  else {

// if everything is filled, insert into database
    $sql = "INSERT INTO products (ksu, name, price, type, attribute)
   VALUES ('$ksu', '$name', '$price', '$type', '$attribute')";

    if (mysqli_query($conn,$sql)) {
      echo"<script>
      alert('INSERT COMPLETED');
      </script>";
                    }
                    else  {
                    echo "<script>alert('FAILED TO INSERT');</script>";
                    }
                    }
}

$conn->close();


?>
